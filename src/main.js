import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

Vue.config.productionTip = false;
Vue.use(VueMaterial);

axios.defaults.baseURL = "http://192.168.1.142:3000";

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
