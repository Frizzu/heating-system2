import axios from 'axios'

export default {
  getComponentsByItemType(idItemType) {
    return axios
      .get("/itemtypes/"+ idItemType +"/items", {
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then(function (response) {
        return response
      })
  },

  createComponent (name, itemTypeId) {
    return axios
      .post('/items/add',
        {
          name,
          url: '',
          itemTypeId
        },
        {
          headers: {
            'Content-Type': 'application/json'
          }
        })
      .then(function (response) {
        return response.data
      })
  },

  updateComponent (idComponent, name) {
    return axios
      .put('/items/update',
        {
          id: idComponent,
          name
        },
        {
          headers: {
            'Content-Type': 'application/json'
          }
        })
      .then(function (response) {
        return response.data
      })
  },

  deleteComponent (idComponent) {
    return axios
      .delete("/items/"+ idComponent +"/delete", {
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then(function (response) {
        return response
      })
  }
}
