import axios from 'axios'

export default {
  getItemValues(idItem) {
    return axios
      .get("/itemcriteria/"+ idItem, {
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then(function (response) {
        return response
      })
  },

  createItemValue(itemValue) {
    return axios
      .post('/itemcriteria/add',
        {
          itemId: itemValue.itemId,
          criteriaId: itemValue.criteriaId,
          value: +itemValue.value
        },
        {
          headers: {
            'Content-Type': 'application/json'
          }
        })
      .then(function (response) {
        return response.data
      })
  },

  updateItemValues(itemValue) {
    return axios
      .put('/itemcriteria/update',
        {
          itemId: itemValue.itemId,
          criteriaId: itemValue.criteriaId,
          value: +itemValue.value
        },
        {
          headers: {
            'Content-Type': 'application/json'
          }
        })
      .then(function (response) {
        return response.data
      })
  }
}
