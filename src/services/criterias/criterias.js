import axios from 'axios'

export default {
  getCriteriasByItemType(idItemType) {
    return axios
      .get("/itemtypes/"+ idItemType +"/criterias", {
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then(function (response) {
        return response
      })
  },

  createCriteria (name, weight, polarity, measureUnit, itemTypeId) {
    return axios
      .post('/criteria/add',
        {
          name,
          weight,
          measureUnit,
          polarity,
          itemTypeId
        },
        {
          headers: {
            'Content-Type': 'application/json'
          }
        })
      .then(function (response) {
        return response.data
      })
  },

  updateCriteria (idCriteria, weight) {
    const numericWeight = +weight;
    return axios
      .put('/criteria/update',
        {
          id: idCriteria,
          weight: numericWeight
        },
        {
          headers: {
            'Content-Type': 'application/json'
          }
        })
      .then(function (response) {
        return response.data
      })
  },

  deleteCriteria (idCriteria) {
    return axios
      .delete("/criteria/"+ idCriteria +"/delete", {
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then(function (response) {
        return response
      })
  }
}
