import axios from 'axios'

export default {
  getItemTypes() {
    return axios
      .get("/itemtypes", {
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then(function (response) {
        return response
      })
  }
}
