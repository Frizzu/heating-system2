import Vue from 'vue'
import Vuex from 'vuex'
import componentsAPI from '../services/components/components'
import criteriasAPI from '../services/criterias/criterias'
import itemTypesAPI from '../services/itemTypes/itemTypes'
import itemValuesAPI from '../services/itemValues/itemValues'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    currentComponent: "pump",
    currentComponentId: 3,
    components: [],
    criterias: [],
    results: [
      {
        type: 'pipes',
        name: "pipes"
      },
      {
        type: 'radiator',
        name: "radiator"
      },
      {
        type: 'pump',
        name: "pump"
      },
      {
        type: 'thermostatic valve',
        name: "tvalve"
      },
      {
        type: 'balancing valve',
        name: "bvalve"
      }
    ],
    isAddCriteriaModalDisplayed: false,
    isAddComponentModalDisplayed: false,
    currentItemName: "",
    currentItemValues: []
  },
  mutations: {
    changeCurrentComponent (state, newComponent) {
      state.currentComponent = newComponent;
      switch (state.currentComponent) {
        case 'pump':
          state.currentComponentId = 3;
          break;
        case 'bvalve':
          state.currentComponentId = 5;
          break;
        case 'tvalve':
          state.currentComponentId = 4;
          break;
        case 'radiator':
          state.currentComponentId = 2;
          break;
        case 'pipe':
          state.currentComponentId = 1;
          break;
        default:
          state.currentComponentId = -1;
      }
    },

    changeAddComponentModalState (state, newState) {
      state.isAddComponentModalDisplayed = newState
    },

    changeAddCriteriaModalState (state, newState) {
      state.isAddCriteriaModalDisplayed = newState
    },

    setComponents (state, components) {
      state.components = components
    },

    setCriterias (state, criterias) {
      state.criterias = criterias;
    },

    setCurrentItemName (state, name) {
      state.currentItemName = name;
    },

    setCurrentItemValues (state, values)  {
      state.currentItemValues = values;
    },

    resetCurrentItem (state) {
      state.currentItemName = "";
      state.currentItemValues = [];
    },

    addCriteria (state, newCriteria) {
      state.criterias.push(newCriteria)
    },

    deleteCriteria (state, idCriteria) {
      const indexOfCriteria = state.criterias.map(criteria => criteria.id).indexOf(idCriteria);
      state.criterias.splice(indexOfCriteria, 1);
    },

    editCriteria (state, criteria) {
      const indexOfCriteria = state.criterias.map(criteria => criteria.id).indexOf(criteria.id);
      state.criterias[indexOfCriteria].weight = criteria.weight;
    },

    addComponent (state, newComponent) {
      state.components.push(newComponent);
    },

    deleteComponent (state, idComponent) {
      const indexOfComponent = state.components.map(component => component.id).indexOf(idComponent);
      state.components.splice(indexOfComponent, 1);
    },

    editComponent (state, component) {
      const indexOfComponent = state.components.map(component => component.id).indexOf(component.id);
      state.components[indexOfComponent].name = component.name;
    }
  },
  actions: {
    initApp({ dispatch }) {
      itemTypesAPI.getItemTypes().then();
      dispatch("getComponentsAndCriteriasByItemType");
    },

    changeCurrentComponent ({ commit, dispatch }, newComponent) {
      commit('changeCurrentComponent', newComponent);
      commit('resetCurrentItem');
      dispatch('getComponentsAndCriteriasByItemType');
    },

    getComponentsAndCriteriasByItemType({ state, commit }) {
      componentsAPI.getComponentsByItemType(state.currentComponentId).then(res => {
        commit("setComponents", res.data)
      });
      criteriasAPI.getCriteriasByItemType(state.currentComponentId).then(res => {
        commit("setCriterias", res.data)
      });
    },

    async getComponentsAndCriteriasByDefinedItemType({ commit }, idItemType) {
      componentsAPI.getComponentsByItemType(idItemType).then(res => {
        commit("setComponents", res.data)
      });
      await criteriasAPI.getCriteriasByItemType(idItemType).then(res => {
        commit("setCriterias", res.data)
      });
    },

    getItemValues({ state, commit }, idItem) {
      return new Promise(function(resolve) {
        itemValuesAPI.getItemValues(idItem).then(res => {
          const idCriteriasInValues = res.data.map(value => value.criteriaId);
          const idCriterias = state.criterias.map(criteria => criteria.id);
          state.criterias.forEach(criteria => {
            if(idCriteriasInValues.indexOf(criteria.id) === -1) {
              res.data.push({
                itemId: idItem,
                criteriaId: criteria.id,
                value: 0,
                created: true
              })
            }
          });
          setTimeout(() => {
            res.data.forEach(value => {
              const indexOfCriteria = idCriterias.indexOf(value.criteriaId);
              if(indexOfCriteria > -1) {
                value.criteriaName = state.criterias[indexOfCriteria].name;
                value.criteriaMeasure = state.criterias[indexOfCriteria].measureUnit;
              }
            });
          }, 500);
          setTimeout(() => {
            commit("setCurrentItemValues", res.data);
            resolve(res.data);
          }, 1500);
        })
      });
    },

    createItemValue({}, value) {
      itemValuesAPI.createItemValue(value);
    },

    updateItemValue({}, value) {
      itemValuesAPI.updateItemValues(value);
    },

    addCriteria({ state, commit }, newCriteria) {
      const polarity = newCriteria.polarity === '+';
      criteriasAPI.createCriteria(newCriteria.name, newCriteria.weight, polarity, newCriteria.measure, state.currentComponentId).then();
      newCriteria.polarity = newCriteria.polarity === '+';
      commit('addCriteria', newCriteria);
    },

    deleteCriteria({ commit }, idCriteria) {
      criteriasAPI.deleteCriteria(idCriteria).then();
      commit('deleteCriteria', idCriteria)
    },

    editCriteria({ commit }, criteria) {
      criteriasAPI.updateCriteria(criteria.id, criteria.weight).then();
      commit('editCriteria', criteria);
    },

    addComponent({ state, commit }, newComponent){
      componentsAPI.createComponent(newComponent.name, state.currentComponentId);
      commit('addComponent', newComponent);
    },

    deleteComponent({ commit }, idComponent) {
      componentsAPI.deleteComponent(idComponent).then();
      commit('deleteComponent', idComponent)
    },

    editComponent({ commit }, component) {
      componentsAPI.updateComponent(component.id, component.name).then();
      commit('editComponent', component);
    }
  },
  modules: {
  }
})
