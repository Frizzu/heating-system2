import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import ComponentPage from "../views/ComponentPage";
import ResultPage from "../views/ResultPage";
import AddComponent from "../views/AddComponent";

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    redirect: '/component'
  },
  {
    path: '/component',
    name: 'Components',
    component: ComponentPage
  },
  {
    path: '/result',
    name: 'Result',
    component: ResultPage
  },
  {
    path: '/addComponent',
    name: 'AddComponent',
    component: AddComponent
  }
];

const router = new VueRouter({
  routes
});

export default router
